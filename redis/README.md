## Redis
> [Redis](https://github.com/redis/redis) is often referred to as a data structures server.

![pulls](https://img.shields.io/docker/pulls/huiyifyj/redis?logo=docker&style=flat-square&label=pulls)

#### Install
```shell
docker pull huiyifyj/redis
```

#### Usage
```shell
docker run -p 6379:6379 -v -d \
    $PWD/data:/data huiyifyj/redis
```
