# compile build stage
FROM debian:bullseye-slim AS builder

ENV REDIS_VERSION 6.2.5
ENV PATH /usr/local/redis/bin:$PATH

RUN set -eux; \
	buildDeps="ca-certificates curl dpkg-dev gcc libc6-dev libssl-dev make"; \
	apt-get update && apt-get install -y --no-install-recommends $buildDeps; \
	curl -sSL "http://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz" -o redis.tar.gz; \
	mkdir -p /usr/src/redis; \
	tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1; \
	cd /usr/src/redis; \
	# disable Redis protected mode [1] as it is unnecessary in context of Docker
	# (ports are not automatically exposed when running inside Docker, but rather explicitly by specifying -p / -P)
	# [1]: https://github.com/redis/redis/commit/edd4d555df57dc84265fdfb4ef59a4678832f6da
	grep -E '^ *createBoolConfig[(]"protected-mode",.*, *1 *,.*[)],$' /usr/src/redis/src/config.c; \
	sed -ri 's!^( *createBoolConfig[(]"protected-mode",.*, *)1( *,.*[)],)$!\10\2!' /usr/src/redis/src/config.c; \
	grep -E '^ *createBoolConfig[(]"protected-mode",.*, *0 *,.*[)],$' /usr/src/redis/src/config.c; \
	# for future reference, we modify this directly in the source instead of just supplying a default configuration flag,
	# because apparently "if you specify any argument to redis-server, [it assumes] you are going to specify everything"
	# see also https://github.com/docker-library/redis/issues/4#issuecomment-50780840
	# (more exactly, this makes sure the default behavior of "save on SIGTERM" stays functional by default)
	\
	# build with TLS support `make BUILD_TLS=yes` (must apt install libssl1.1)
	make && make install; \
	mkdir -p /usr/local/redis/bin; \
	cp src/redis-benchmark src/redis-cli src/redis-server /usr/local/redis/bin/; \
	cd /usr/local/redis/bin; \
	ln -s redis-server redis-check-aof; \
	ln -s redis-server redis-check-rdb; \
	ln -s redis-server redis-sentinel; \
	# smoke test
	redis-cli --version; \
	redis-server --version

# main stage
FROM debian:bullseye-slim

WORKDIR /data

COPY --from=builder /usr/local/redis/bin/ /usr/local/bin/

VOLUME /data

EXPOSE 6379

CMD ["redis-server"]
