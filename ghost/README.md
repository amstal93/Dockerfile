<img src="../image/ghost.png" width="200px">

## Ghost for docker

![pulls](https://img.shields.io/docker/pulls/huiyifyj/ghost?logo=docker&style=flat-square&label=pulls)

#### Install
```shell
docker pull huiyifyj/ghost
```

#### Usage
##### via [docker-compose](https://github.com/docker/compose)
Example `docker-compose.yml` for ghost v4:
```yaml
# by default, the Ghost image will use SQLite (and thus requires no separate database container)
# we have used MySQL here merely for demonstration purposes (especially environment-variable-based configuration)
version: '3.1'

services:
  ghost:
    image: huiyifyj/ghost:latest
    container_name: ghost
    restart: always
    ports:
      - 3000:2368
    environment:
      # this url value is just an example, and is likely wrong for your environment!
      url: http://localhost:3000
      # see https://ghost.org/docs/config/#configuration-options
      database__client: mysql
      database__connection__host: 127.0.0.1
      database__connection__port: 3306
      database__connection__user: ghost
      database__connection__password: ghost_pass
      database__connection__database: ghost_database
      # listen to all ips
      # server__host: 0.0.0.0
      # this image defaults to NODE_ENV=productio.
      # so development mode needs to be explicitly specified if desired.
      # NODE_ENV: development
      TZ="Asia/Shanghai"
```
Run `docker-compose up -d`, wait for it to initialize completely, and visit http://localhost:3000.

##### via docker cli shell (Not recommended)
```shell
docker run -d --name ghost -p 3000:2368 \
    -v ./ghost/content:/ghost/content \
    -e url=http://some-ghost.example.com <others> \
    huiyifyj/ghost
```
