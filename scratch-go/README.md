## Go Binary on Docker Scratch

![pulls](https://img.shields.io/docker/pulls/huiyifyj/scratch-go?logo=docker&style=flat-square&label=pulls)

#### Install
```shell
docker pull huiyifyj/scratch-go
```

#### Usage
```shell
docker run -p 7991:7991 huiyifyj/scratch-go
```

Run it, wait for it to initialize completely, and visit http://localhost:7991.
