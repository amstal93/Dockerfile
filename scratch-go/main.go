package main

import (
	"fmt"
	"io"
	"net/http"
	"os/user"
	"runtime"
)

func hello(w http.ResponseWriter, r *http.Request) {
	home, _ := user.Current()
	buildInfo := fmt.Sprintf(`%s, Hello world!

----------------------------------
Build information:
- Go version: %s
- OS/Arch:    %s/%s
----------------------------------
More to visit the following links
- https://hub.docker.com/r/huiyifyj/scratch-go
- https://gitlab.com/huiyifyj/Dockerfile/-/tree/master/scratch-go
`, home.Username, runtime.Version(), runtime.GOOS, runtime.GOARCH)
	io.WriteString(w, buildInfo)
}

func main() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":7991", nil)
}
